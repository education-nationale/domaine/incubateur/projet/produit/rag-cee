import logging
from os import listdir
from os.path import isfile, join

from pymilvus import DataType  # type: ignore
from rag.common.logger import logger
from rag.common.variable import (
    ALBERT_API_KEY,
    ALBERT_CHUNKS_LIMIT,
    ALBERT_OPEN_AI_URL,
    EMBEDDING_MODEL,
)
from rag.include.chunking import chunk
from rag.include.metadata import extract_metadata
from rag.include.openai import generate_embeddings
from rag.vectorstore.milvus import CustomMilvusClient

# Configuration
CHUNK_SIZE = 1000
CHUNK_OVERLAP = 250
DATA_FOLDER = "rag/vectorstore/data/"
COLLECTION_NAME = "rapports_etablissements"
MILVUS_HOST = "http://rag-milvus-standalone"
MILVUS_PORT = "19530"

config = {
    "fields": [
        {"field_name": "id", "datatype": DataType.INT64},
        {"field_name": "id", "datatype": DataType.INT64},
    ]
}


# Create vectorstore
def initialize_vector_store(collection_name: str) -> None:
    custom_milvus_client = CustomMilvusClient(
        milvus_host=MILVUS_HOST,
        milvus_port=MILVUS_PORT,
        collection_name=collection_name,
    )
    custom_milvus_client.delete_collection()
    custom_milvus_client.create_collection_if_not_exists(
        vector_field_name="embedding", metric_type="IP", index_type="FLAT"
    )


# Insert document in vectorstore
def process_document(
    file_path: str, chunk_size: int = CHUNK_SIZE, chunk_overlap: int = CHUNK_OVERLAP
) -> None:
    logging.info("Extracting metadata...")
    custom_metadata = extract_metadata(file_path)

    logging.info("Splitting doc into chunks...")
    docs = chunk(file_path, chunk_size=chunk_size, chunk_overlap=chunk_overlap)

    logging.info("Generating embeddings...")
    # When docs is an image
    if len(docs) == 0:
        pass
    embeddings = []
    for doc in range(0, len(docs), ALBERT_CHUNKS_LIMIT):
        embeddings.extend(
            generate_embeddings(
                [d.page_content for d in docs[doc : doc + ALBERT_CHUNKS_LIMIT]],
                model=EMBEDDING_MODEL,
                api_key=ALBERT_API_KEY,
                api_url=ALBERT_OPEN_AI_URL,
            )
        )

    for d in docs:
        d.metadata.update(custom_metadata)

    logging.info("Inserting data into Milvus vectorstore...")
    custom_milvus_client = CustomMilvusClient(
        milvus_host=MILVUS_HOST,
        milvus_port=MILVUS_PORT,
        collection_name=COLLECTION_NAME,
    )
    custom_milvus_client.insert_data(docs=docs, embeddings_list=embeddings)


if __name__ == "__main__":
    logger()

    initialize_vector_store(COLLECTION_NAME)
    report_files = [
        f
        for f in listdir(DATA_FOLDER)
        if isfile(join(DATA_FOLDER, f))
        if f.endswith(".pdf")
    ]
    for report in report_files:
        logging.info(f"Processing file {report}")
        process_document(DATA_FOLDER + report)

    try:
        test_search_client = CustomMilvusClient(
            milvus_host=MILVUS_HOST,
            milvus_port=MILVUS_PORT,
            collection_name=COLLECTION_NAME,
        )
        result = test_search_client.search_similar_chunks(query_text="Test")
    except:
        raise

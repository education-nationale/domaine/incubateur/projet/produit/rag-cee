import math


def single_query_metrics(
    list_obj: list[str], list_retriever: list[str]
) -> tuple[float, float, float, float]:
    """
    Pour une question unique :
    Utilise une liste de chunks (list_obj) objectifs (id de chunks intéressants pour la question)
    Utilise une liste de chunks (list_retriever) récupérés par le retriever
    """
    sim = set(list_obj).intersection(list_retriever)
    precision = (len(sim) / len(list_retriever)) if list_retriever else 0
    recall = (len(sim) / len(list_obj)) if list_obj else 0
    dcg, idcg = 0.0, 0.0
    for i, doc in enumerate(list_retriever):
        if doc in list_obj:
            dcg += 1 / math.log2(i + 2)
    for i in range(len(list_obj)):
        idcg += 1 / math.log2(i + 2)
    ndcg = dcg / idcg if idcg else 0.0
    rr = 0.0
    for i, doc in enumerate(list_retriever):
        if doc in list_obj:
            rr = 1 / (i + 1)
            break
    return precision, recall, ndcg, rr


def metrics(
    list_of_obj: list[list[str]], list_of_retrievers: list[list[str]]
) -> tuple[float, float, float, float]:
    """
    Pour une liste de questions, pour tous les dataset :
    Utilise une liste de chunks (list_obj) objectifs (id de chunks intéressants pour la question)
    Utilise une liste de chunks (list_retriever) récupérés par le retriever
    """
    p, r, n, rr_sum = 0.0, 0.0, 0.0, 0.0
    for o, t in zip(list_of_obj, list_of_retrievers):
        pr, re, nd, rr = single_query_metrics(o, t)
        p += pr
        r += re
        n += nd
        rr_sum += rr
    c = len(list_of_obj)
    return round(p / c, 2), round(r / c, 2), round(n / c, 2), round(rr_sum / c, 2)

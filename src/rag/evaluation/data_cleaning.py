import re
from typing import Any, List

import pandas as pd

from rag.common.variable import EVALUATION_CSV_PATH
from rag.vectorstore.milvus import CustomMilvusClient
from rag.common.variable import COLLECTION_NAME, EVALUATION_CSV_PATH

def expand_page_values(df: pd.DataFrame) -> pd.DataFrame:
    expanded_rows: list[dict[str, Any]] = []

    for _, row in df.iterrows():
        page_value = str(row["page"]).strip()

        if page_value == "0":
            continue

        if page_value.upper() == "NR":
            row_copy = row.copy().to_dict()
            row_copy["page"] = None
            expanded_rows.append(row_copy)
            continue

        if re.match(r"^\d+\s*(?:à|-)\s*\d+$", page_value):
            start, end = map(int, re.findall(r"\d+", page_value))
            for p1 in range(start, end + 1):
                row_copy = row.copy().to_dict()
                row_copy["page"] = p1
                expanded_rows.append(row_copy)
            continue

        pages: List[str] = re.split(r"[,\s;et]+", page_value)
        for p2 in pages:
            if p2.isdigit():
                row_copy = row.copy().to_dict()
                row_copy["page"] = p2
                expanded_rows.append(row_copy)

    return pd.DataFrame(expanded_rows)


def clean_and_process_csv(file_path: str) -> pd.DataFrame:
    print("Open csv file")
    df = pd.read_csv(file_path)
    print(f"Read source csv DF:{df}")

    df["Nom du rapport"] = df["Nom du rapport"].replace("", None).ffill()

    df_cleaned = df[["Question", "Nom du rapport", "Page du rapport"]].rename(
        columns={
            "Nom du rapport": "name",
            "Page du rapport": "page",
            "Question": "question",
        }
    )
    print(f"Cleaned csv DF:{df_cleaned}")

    df_out = expand_page_values(df_cleaned)
    # Ajout des chunk_id pour chaque ligne, en filtrant sur le nom du doc, et sur la page
    custom_milvus_client = CustomMilvusClient(
          milvus_host="http://localhost",
          milvus_port="19530",
          collection_name=COLLECTION_NAME,
      )

    # Créer une liste pour stocker les résultats
    results_list = []

    # Parcourir chaque ligne du DataFrame et ajouter le résultat à la liste
    for _, line in df_out.iterrows():
        result = custom_milvus_client.query_chunk_id(
            filter=f"metadata LIKE '%{line['name']}%' AND metadata LIKE '%\"page\": {line['page']},%'"
        )
        results_list.append(result)

    # Ajouter la liste comme nouvelle colonne au DataFrame
    df_out['chunk_ids'] = results_list

    return df_out



if __name__ == "__main__":
    # https://grist.numerique.gouv.fr/o/docs/bhMFHNv222Mb/Questions-test-IA/p/1
    input_file = EVALUATION_CSV_PATH + 'questions_grist.csv'


    df_final = clean_and_process_csv(input_file)
    print(df_final)
    print(f"Writing data to csv: df_cleaned.csv")
    df_final.to_csv(EVALUATION_CSV_PATH+"df_cleaned.csv", index=False)

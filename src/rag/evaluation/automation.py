import json
import ast
import os
import argparse
from datetime import datetime

import pandas as pd
from rag.common.variable import COLLECTION_NAME, EVALUATION_CSV_PATH
from rag.evaluation.metrics import single_query_metrics
from rag.vectorstore.milvus import CustomMilvusClient


# def flatten(nested_list):
#     result = []
#     for sublist in nested_list:
#         result.extend(sublist)  # ajoute chaque élément de sublist à result
#     return result




# TODO DRAFT : à adapter une fois la base ajoutée en prod pour des correctifs
def automation_metrics() -> pd.DataFrame:
    df = pd.read_csv(EVALUATION_CSV_PATH+"df_cleaned.csv")
    df = df[df["chunk_ids"].apply(lambda x: len(x) > 0)]
    df['chunk_ids'] = df['chunk_ids'].apply(ast.literal_eval)

    df = df.groupby(["question","name"]).agg(
        {"page": list,
         "chunk_ids": lambda x: [item for sublist in x for item in sublist]}
         ).reset_index()

    custom_milvus_client = CustomMilvusClient(
        milvus_host="http://localhost",
        milvus_port="19530",
        collection_name=COLLECTION_NAME,
    )

    def extract_info(row: dict[str, str]) -> tuple[list[str], list[str], list[str]]:
        print(row["question"])
        search_results = custom_milvus_client.search_similar_chunks(
            query_text=row["question"],
            filter=f"metadata LIKE '%{row['name']}%'"
         ) # En condition réelle le retriever aura l'identifiant de l'établissement pour filtrer
        ids = []

        for result in search_results:
            ids.append(result.metadata["id"])

        return ids

    df["extract_id"] = df.apply(extract_info,axis=1)

    def calculate_row_metrics(row: dict[str, list[str]]) :
        id_metrics = single_query_metrics(row["chunk_ids"], row["extract_id"])

        return pd.Series(
            {
                "id_precision": id_metrics[0],
                "id_recall": id_metrics[1],
                "id_ndcg": id_metrics[2],
                "id_rr": id_metrics[3],
            }
        )

    df = df.join(df.apply(calculate_row_metrics, axis=1))

    return df

# Fonction pour mettre à jour le fichier Markdown
def update_markdown_file(df, commit_message=""):
    folder_path = "src/rag/evaluation"
    filename = os.path.join(folder_path, "performance_history.md")
    # Créer l'en-tête si le fichier n'existe pas
    if not os.path.exists(filename):
        with open(filename, 'w', encoding='utf-8') as f:
            f.write("# Historique des performances\n\n")
            f.write("| Date | Avg Precision | Avg Recall | Avg NDCG moyen | AVG RR | Best result |\n")
            f.write("|------|------------------|--------------|------------|----------|-------------------|\n")

    # Ajouter la nouvelle ligne
    with open(filename, 'a', encoding='utf-8') as f:
        for _, row in df.iterrows():
            f.write(f"| {row['Date']} | {row['average_precision']:.4f} | {row['average_recall']:.4f} | ")
            f.write(f"{row['average_ndcg']:.4f} | {row['average_rr']:.4f} | {row['best_result']} |\n")

        # Ajouter le message de commit s'il existe
        if commit_message:
            f.write(f"\n> Commit du {datetime.now().strftime('%Y-%m-%d %H:%M:%S')}: {commit_message}\n\n")


print("Fichier Markdown mis à jour avec succès!")

def store_results(df):
  average_metrics = {
        'Date': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        'average_precision': df['id_precision'].mean(),
        'average_recall': df['id_recall'].mean(),
        'average_ndcg': df['id_ndcg'].mean(),
        'average_rr': df['id_rr'].mean()
        }
  # Trouver la meilleure ligne en fonction du rappel
  best_idx = df['id_recall'].idxmax()
  best_result = {
      'recall': df.loc[best_idx, 'id_recall'],
      'precision': df.loc[best_idx, 'id_precision'],
      'ndcg': df.loc[best_idx, 'id_ndcg'],
      'rr': df.loc[best_idx, 'id_rr'],
      'question': df.loc[best_idx, 'question'],
      'name': df.loc[best_idx, 'name'],
      'page': df.loc[best_idx, 'page'],
      'chunk_id': df.loc[best_idx, 'chunk_ids'],
      'extract_id': df.loc[best_idx, 'extract_id']
  }
  # Ajouter les informations du meilleur résultat
  average_metrics['best_result'] = str(best_result)

  # Créer un dataframe avec les moyennes
  result_df = pd.DataFrame([average_metrics])

  return result_df


def main():
  # Configurer l'analyseur d'arguments
  parser = argparse.ArgumentParser(description="Mettre à jour l'historique des performances avec un message de commit")
  parser.add_argument("--commit-message", "-m", type=str, default="",
                      help="Message de commit associé à cette mise à jour")
  args = parser.parse_args()

  # Récupérer le message de commit
  commit_message = args.commit_message
  df = automation_metrics()
  result_df = store_results(df)
  update_markdown_file(result_df,commit_message=commit_message)

if __name__ == "__main__":
    # executer python automation.py --commit-message="première utilisation des métriques"
    main()




import logging
from typing import Any

from fastapi import FastAPI
from pydantic import BaseModel
from rag.common.variable import ALBERT_API_KEY, ALBERT_OPEN_AI_URL, SMALL_LLM_MODEL
from rag.include.openai import generate_response
from rag.vectorstore.milvus import CustomMilvusClient

app = FastAPI()

logger = logging.getLogger(__name__)


class Query(BaseModel):
    text: str
    filter: str
    prompt_history: list[dict[str, str]]
    message_id: str  # TODO: modifier pour un int


@app.get("/")
async def root() -> dict[str, str]:
    return {"message": "Hello World"}


@app.post("/query")
async def process_query(query: Query) -> dict[str, Any]:
    milvus_client = CustomMilvusClient(
        milvus_host="http://rag-milvus-standalone",
        milvus_port="19530",
        collection_name="rapports_etablissements",
    )
    relevant_chunks = milvus_client.search_similar_chunks(
        query_text=query.text, filter=query.filter
    )
    logging.info("Relevant chunks retrieved")

    # TODO: créer une méthode pour que le query_prompt soit paramétrisable

    query_prompt = "Voici le contexte : \n"

    for doc in relevant_chunks:
        query_prompt += doc.page_content + "\n\n"

    query_prompt += f"Voici la question de l'utilisateur : {query.text}"

    response = generate_response(
        prompt_history=query.prompt_history,
        query_prompt=query_prompt,
        #  chunks=relevant_chunks,
        model=SMALL_LLM_MODEL,
        api_key=ALBERT_API_KEY,
        api_url=ALBERT_OPEN_AI_URL,
    )

    return {"llm_response": response}


# TODO : renvoyer le chunk, les metada pour sourcer le retour

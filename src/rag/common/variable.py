import os

from dotenv import load_dotenv

load_dotenv()
ALBERT_API_KEY = str(os.getenv("ALBERT_API_KEY"))
EMBEDDING_MODEL = str(os.getenv("EMBEDDING_MODEL"))
ALBERT_OPEN_AI_URL = str(os.getenv("ALBERT_OPEN_AI_URL"))
SMALL_LLM_MODEL = str(os.getenv("SMALL_ALBERT_LANGUAGE_MODEL"))
LARGE_LLM_MODEL = str(os.getenv("LARGE_ALBERT_LANGUAGE_MODEL"))
RAG_MILVUS_DB_URL = str(os.getenv("RAG_MILVUS_DB_URL"))
ALBERT_CHUNKS_LIMIT = int(str(os.getenv("ALBERT_CHUNKS_LIMIT")))
EVALUATION_CSV_PATH=str(os.getenv("EVALUATION_CSV_PATH"))

CHUNK_SIZE = 1000
CHUNK_OVERLAP = 250
DATA_FOLDER = "rag/vectorstore/data/"
COLLECTION_NAME = "rapports_etablissements"
MILVUS_HOST = "http://rag-milvus-standalone"
MILVUS_PORT = "19530"

PROMPT_SYSTEM = """
On vous donne, sur une demande utilisateur, du contexte textuel et des règles, le tout à l’intérieur de balises XML.
Vous devez répondre à la requête en vous basant sur le contexte tout en respectant les règles.

- Si vous ne savez pas, dites-le simplement.
- Si vous n’êtes pas sûr, demandez des précisions.
- Répondez dans la même langue que la requête de l’utilisateur.
- Si le contexte semble illisible ou de mauvaise qualité, informez l’utilisateur puis répondez du mieux que vous pouvez.
- Répondez directement
"""

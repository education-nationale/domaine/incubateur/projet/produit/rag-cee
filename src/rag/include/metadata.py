import logging
import os
import re


def extract_uai_from_filename(filename: str) -> str:
    match = re.search(r"(\d{7}[A-Z])", filename)
    return match.group(1) if match else "UNKNOWN"


def extract_metadata(filename: str) -> dict[str, str]:
    logging.info(f"Extracting metadata for file {filename}")
    base_name = os.path.basename(filename)
    uai = extract_uai_from_filename(base_name)
    logging.info(f"UAI for this file is {uai}")
    return {"filename": base_name, "uai": uai}

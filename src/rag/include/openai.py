# Connecteur openAI
from typing import Any

import requests
from rag.common.variable import PROMPT_SYSTEM


def generate_response(
    prompt_history: list[dict[str, str]],
    query_prompt: str,
    model: str,
    api_key: str,
    api_url: str,
) -> Any:
    messages = [
        {
            "content": PROMPT_SYSTEM,
            "role": "system",
        }
    ]
    messages.extend(prompt_history)
    messages.append(
        {
            "content": query_prompt,
            "role": "user",
        }
    )
    response = requests.post(
        api_url + "chat/completions",
        json={
            "messages": messages,
            "model": model,
        },
        headers={
            "authorization": f"Bearer {api_key}",
            "content-type": "application/json",
        },
    )

    return response.json()["choices"][0]["message"]["content"]


def generate_embeddings(
    docs: list[str], model: str, api_key: str, api_url: str
) -> list[dict[str, list[float]]]:
    response = requests.post(
        api_url + "embeddings",
        headers={
            "Authorization": f"Bearer {api_key}",
            "Content-Type": "application/json",
        },
        json={"input": docs, "model": model},
    )

    response.raise_for_status()
    result: list[dict[str, list[float]]] = response.json().get("data")
    return result

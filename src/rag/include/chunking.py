import logging

from langchain_community.document_loaders import PyPDFLoader  # type: ignore
from langchain_core.documents import Document  # type: ignore
from langchain_text_splitters import RecursiveCharacterTextSplitter  # type: ignore

# TODO: Improve conversion using pdf to md format
# from markitdown import MarkItDown
# markitdown = MarkItDown()
# result = markitdown.convert("document.pdf")
# print(result.text_content)


def chunk(
    file_path: str | None = None,
    docs: Document | None = None,
    chunk_size: int = 1000,
    chunk_overlap: int = 250,
) -> Document:
    if file_path is not None:
        loader = PyPDFLoader(file_path)
        docs = loader.load()
    if docs is not None:
        docs = docs
    else:
        raise ValueError("Either doc or filepath have to be provided")

    # Using a recursive splitter strategy
    splitter = RecursiveCharacterTextSplitter(
        chunk_size=chunk_size, chunk_overlap=chunk_overlap
    )
    docs = splitter.split_documents(docs)
    logging.info(f"Splitted doc into {len(docs)} chunks")

    return docs

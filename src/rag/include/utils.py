import os


def get_env(
    variable: str, default: str | None = None, required: bool = True
) -> str | None:
    value = os.getenv(variable, default)
    if required and value is None:
        raise ValueError(
            f"Variable {variable} not found and no default value available: failing"
        )
    return value


def get_env_str(variable: str, default: str | None = None) -> str:
    # This function is an alternative version of get_env. In get_env, it is allowed to return either a string
    # or None. However, sometimes it can be counter-productive to type "str | None" when it can only be a string value.
    # Mypy cannot detect it due to get_env's implementation logic.
    # This is why we cast as str for mypy. We know for sure it's a str thanks to required=True.
    return str(get_env(variable, default))

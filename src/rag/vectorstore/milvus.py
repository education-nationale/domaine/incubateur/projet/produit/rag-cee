import ast
import json
import logging

from langchain_core.documents import Document  # type: ignore
from pymilvus import DataType, MilvusClient  # type: ignore[import-untyped]
from rag.common.variable import ALBERT_API_KEY, ALBERT_OPEN_AI_URL, EMBEDDING_MODEL
from rag.include.openai import generate_embeddings


class CustomMilvusClient(MilvusClient):
    def __init__(
        self,
        milvus_host: str,
        milvus_port: str,
        collection_name: str,
        config: None = None,
    ) -> None:  # TODO: Ajouter une config pour que tout soit paramétrisable
        self.milvus_client = MilvusClient(
            uri=milvus_host + ":" + milvus_port, token="root:Milvus"
        )
        self.collection_name = collection_name
        self.config = config

    def delete_collection(self) -> None:
        self.milvus_client.drop_collection(self.collection_name)
        logging.warning(f"Collection {self.collection_name} dropped")

    def create_collection_if_not_exists(
        self, vector_field_name: str, metric_type: str, index_type: str
    ) -> bool:
        if self.milvus_client.has_collection(self.collection_name):
            logging.info(f"Collection {self.collection_name} already exists")
            return False
        # Définition du schéma
        schema = self.milvus_client.create_schema(
            auto_id=True, enable_dynamic_field=False
        )
        schema.add_field(field_name="id", datatype=DataType.INT64, is_primary=True)
        schema.add_field(
            field_name="content", datatype=DataType.VARCHAR, max_length=65535
        )
        schema.add_field(
            field_name=vector_field_name, datatype=DataType.FLOAT_VECTOR, dim=1024
        )
        schema.add_field(
            field_name="metadata", datatype=DataType.VARCHAR, max_length=65535
        )

        self.milvus_client.create_collection(
            collection_name=self.collection_name, schema=schema
        )
        self.insert_index(
            field_name=vector_field_name, index_type=index_type, metric_type=metric_type
        )

        logging.info(f"Collection {self.collection_name} created")
        return True

    def insert_index(self, field_name: str, metric_type: str, index_type: str) -> bool:
        # Récupération des index
        if not self.milvus_client.list_indexes(collection_name=self.collection_name):
            index_params = self.milvus_client.prepare_index_params()
            index_params.add_index(
                field_name=field_name,
                metric_type=metric_type,  # Produit scalaire
                index_type=index_type,  #
                index_name=field_name + "_index",
            )

            self.milvus_client.create_index(
                collection_name=self.collection_name, index_params=index_params
            )
            logging.info(
                f"Collection {self.collection_name} has now {field_name}_index as an index"
            )
            return True

        logging.info(f"Collection {self.collection_name} already has an index")
        return True

    def insert_data(
        self, docs: list[Document], embeddings_list: list[dict[str, list[float]]]
    ) -> None:
        # En entrée elle reçoit la liste des chunks avec la liste des embeddings
        # Elle met en forme la donnée :
        # data=[{"embedding": [0.3580376395471989, -0.6023495712049978, 0.18414012509913835,
        #  -0.26286205330961354, 0.9029438446296592], "content": "pink_8682"},
        # Elle envoie la donnée dans la base
        data = []

        for doc, embedding in zip(docs, embeddings_list):
            # Préparation des données
            data.append(
                {
                    "embedding": embedding["embedding"],
                    "content": doc.page_content,
                    "metadata": json.dumps(doc.metadata),
                }
            )

        res = self.milvus_client.insert(collection_name=self.collection_name, data=data)

        logging.info(f"Inserted {res['insert_count']} in collection")

    # TODO: mettre en place les sparse embeddings pour améliorer la recherche
    # def sparse_embeddings(self,chunk_content):
    #   # there are some built-in analyzers for several languages, now we use 'en' for English.
    #   analyzer = build_default_analyzer(language="fr")
    #   tokens = analyzer(chunk_content) # converti le contenu d'un chunk en tokens
    #   bm25_ef = BM25EmbeddingFunction(analyzer)
    #   bm25_ef.fit(corpus)

    def search_similar_chunks(
        self, query_text: str, metric_type: str = "IP", limit: int = 10, filter: str = ""
    ) -> Document:
        query_embeddings = generate_embeddings(
            docs=[query_text],
            model=EMBEDDING_MODEL,
            api_key=ALBERT_API_KEY,
            api_url=ALBERT_OPEN_AI_URL,
        )

        self.milvus_client.load_collection(collection_name=self.collection_name)

        # Search the best matching vector in the Milvus DB collection
        res = self.milvus_client.search(
            collection_name=self.collection_name,
            anns_field="embedding",
            data=[query_embeddings[0]["embedding"]],
            limit=limit,
            filter=filter,
            output_fields=["content", "metadata","id"],
            search_params={"metric_type": metric_type},
        )
        # Regenerate a list of document to have more understable data
        documents = []
        for hits in res[0]:
            metadata = ast.literal_eval(hits["entity"]["metadata"])
            metadata.update({"id": hits["entity"]["id"]})
            documents.append(
                Document(
                    page_content=hits["entity"]["content"],
                    metadata=metadata
                )
            )

        return documents

    def query_chunk_id(
        self, filter: str = ""
    ) -> list:
        # Search the vector ids based on basic filter

        self.milvus_client.load_collection(collection_name=self.collection_name)

        res = self.milvus_client.query(
            collection_name=self.collection_name,
            filter=filter,
            output_fields=["id"],
        )
        chunks_id = []
        for hits in res:
            chunks_id.append(hits["id"])

        return chunks_id

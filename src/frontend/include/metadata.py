from typing import Mapping, cast

import requests


def fetch_communes(codeDepartement: str) -> list[dict[str, str]]:
    response = requests.get(
        f"https://geo.api.gouv.fr/communes?codeDepartement={codeDepartement}&fields=nom,code&format=json"
    )  # TODO: add this as a variable
    if response.status_code == 200:
        return cast(list[dict[str, str]], response.json())
    return [{"": ""}]


def build_text_condition(champ: str, valeurs: list[str]) -> str | None:
    if not valeurs or valeurs == [""]:
        return None
    conditions = [f"{champ} LIKE '{valeur}'" for valeur in valeurs]
    return " OR ".join(conditions)


def fetch_uai_etablissement_filter(
    range: dict[str, int],
    libelles_academies: list[str] = [""],
    libelles_departements: list[str] = [""],
    noms_communes: list[str] = [""],
) -> list[str]:
    # TODO: add this as a variable
    base_url = "https://data.education.gouv.fr/api/v2/catalog/datasets/fr-en-annuaire-education/records"

    # TODO: ajouter statut_public_prive dans les filtres
    # Variable pour public/prive : statut_public_prive:"Privé"
    # type_contrat_prive : HORS CONTRAT/CONTRAT D'ASSOCIATION TOUTES CLASSES/

    # Par ordre de priorité par inclusivité des éléments listés
    criteres = [
        ("nom_commune", noms_communes),
        ("libelle_departement", libelles_departements),
        ("libelle_academie", libelles_academies),
    ]

    condition_text = "N/A"
    for champ, valeurs in criteres:
        if condition_odsql := build_text_condition(champ, valeurs):
            condition_text = condition_odsql
            break

    if condition_text == "N/A":
        return []

    condition_effectif = f"""(nombre_d_eleves > {range['min']}
                            AND nombre_d_eleves < {range['max']}
                            OR nombre_d_eleves IS NULL)"""

    params: Mapping[str, str | int]
    params = {
        "where": condition_text + " AND " + condition_effectif,
        "order_by": "identifiant_de_l_etablissement",
        "limit": 100,
    }

    response = requests.get(base_url, params=params)

    uai_etablissements = [
        record["record"]["fields"]["identifiant_de_l_etablissement"]
        for record in response.json()["records"]
    ]

    return uai_etablissements


def fetch_uai_etablissement_ips_filter(value: int, operator: str) -> list[str]:
    # TODO: add those urls as variables
    # base_url_ecoles = "https://data.education.gouv.fr/api/v2/catalog/datasets/fr-en-ips-ecoles-ap2022/records"
    # base_url_colleges = "https://data.education.gouv.fr/api/v2/catalog/datasets/fr-en-ips-colleges-ap2023/records"
    # base_url_lycees = "https://data.education.gouv.fr/api/v2/catalog/datasets/fr-en-ips-lycees-ap2023/records"
    condition = "N/A"
    if value and operator:
        condition = f"ips {operator} {value}"
    if condition == "N/A":
        return ["TODO"]
    return ["TODO"]


def fetch_uai_etablissement_indice_eloignement_filter(
    value: int, operator: str
) -> list[str]:
    # TODO: add this as a variable
    # base_url = "https://data.education.gouv.fr/api/v2/catalog/datasets/fr-en-annuaire-education/records"  $
    # condition = "N/A"
    return ["TODO"]

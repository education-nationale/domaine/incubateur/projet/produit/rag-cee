import json
import time
from contextlib import contextmanager
from typing import Any, Iterator, Optional

import mysql.connector
from frontend.common.variable import (
    DATABASE_HOST,
    DATABASE_NAME,
    DATABASE_PORT,
    DATABASE_PWD,
    DATABASE_USER,
)
from mysql.connector.abstracts import MySQLConnectionAbstract
from mysql.connector.pooling import PooledMySQLConnection


class DatabaseConnection:
    def __init__(self) -> None:
        self.config = {
            "host": DATABASE_HOST,
            "port": DATABASE_PORT,
            "database": DATABASE_NAME,
            "user": DATABASE_USER,
            "password": DATABASE_PWD,
        }

    @contextmanager
    def get_connection(
        self,
    ) -> Iterator[PooledMySQLConnection | MySQLConnectionAbstract]:
        conn = mysql.connector.connect(**self.config)
        try:
            yield conn
        finally:
            conn.close()


class MessageRepository:
    def __init__(self) -> None:
        self.db = DatabaseConnection()

    def insert_message(
        self,
        user_id: int,
        conv_id: int,
        content: str,
        message_type: str,
        meta: dict[str, str | list[str]] | None = None,
    ) -> Optional[int] | bool:  # Optional because lastrowid might be empty
        try:
            with self.db.get_connection() as conn:
                cursor = conn.cursor()

                cursor.execute(
                    """
                    INSERT INTO messages
                    (user_id, conv_id, content, meta, created_at, message_type)
                    VALUES (%s, %s, %s, %s, %s, %s)
                """,
                    (
                        user_id,
                        conv_id,
                        content,
                        json.dumps(meta) if meta else None,
                        int(time.time_ns()),
                        message_type,
                    ),
                )

                conn.commit()
                cursor.close()

                return cursor.lastrowid

        except Exception as e:
            print(f"Erreur d'insertion: {e}")
            return False

    def update_feedback(self, message_id: int, feedback: int | None) -> bool:
        if feedback is not None:
            with self.db.get_connection() as conn:
                cursor = conn.cursor()
                cursor.execute(
                    "UPDATE messages SET feedback = %s WHERE id = %s",
                    (feedback, message_id),
                )
                conn.commit()
                return True
        else:
            return False

    def get_conv_history(
        self,
        conv_id: int,
    ) -> list[dict[str, Any]]:
        try:
            with self.db.get_connection() as conn:
                cursor = conn.cursor()

                cursor.execute(
                    """
                    SELECT content, message_type
                    FROM messages
                    WHERE conv_id = %s
                    ORDER BY created_at DESC
                    """,
                    (conv_id,),
                )

                messages_data = cursor.fetchall()
                conn.commit()
                cursor.close()

                # Construction du dictionnaire de retour
            prompt_messages_list = []

            # Ajout des messages de la conversation
            for content, message_type in messages_data:
                prompt_messages_list.append(
                    {
                        "content": content,
                        "role": message_type,
                    }
                )

            return prompt_messages_list

        except Exception as e:
            print(f"Erreur de récupération de la donnée : {e}")
            return []

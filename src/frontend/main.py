import time
from collections.abc import Iterator
from logging import getLogger
from typing import Optional

import requests
import streamlit as st

from frontend.common.variable import ACADEMIES, RAG_API_URL, SECTEUR, TYPE_ETABLISSEMENT
from frontend.include.database import MessageRepository
from frontend.include.metadata import fetch_communes, fetch_uai_etablissement_filter

log = getLogger("Logger")


def stream_data(text: str) -> Iterator[str]:
    for word in text.split(" "):
        yield word + " "
        time.sleep(0.02)


def display_message_with_feedback(
    content: str,
    message_type: str,
    metadata: dict[str, str | list[str]],
) -> None:
    message_id = client_sql.insert_message(
        user_id=0,
        conv_id=0,
        content=content,
        message_type=message_type,
        meta=metadata,
    )

    if message_type == "user":
        st.markdown(content)

    if message_type == "assistant":
        stream_response: Iterator[str] = stream_data(content)
        st.session_state.messages.append(
            {"role": "assistant", "content": st.write_stream(stream_response)}
        )

        col1, col2 = st.columns([0.9, 0.1])
        with col2:
            st.feedback(
                "thumbs",
                key=f"feedback_{message_id}",  # TODO: renforcer cet ID avec le user_id
                on_change=update_feedback,
                args=(message_id,),
            )


def update_feedback(message_id: int) -> None:
    # Récupérer la valeur du feedback depuis session_state
    feedback_value = st.session_state[f"feedback_{message_id}"]

    if feedback_value is not None:
        db = MessageRepository()
        db.update_feedback(message_id, feedback_value)


# Interface utilisateur
st.header("Discute avec les rapports CEE 💬 📚")
client_sql = MessageRepository()

# Composant sidebar
with st.sidebar:
    selected_academies = st.multiselect(
        label="Académies",
        options=[academie for academie in ACADEMIES],
        placeholder="Choisissez vos académies",
    )
    selected_departements = st.multiselect(
        label="Départements",
        options=[
            departement
            for academie in selected_academies
            if academie in ACADEMIES
            for departement in ACADEMIES[academie]["departements"].keys()
        ],
        format_func=lambda x: ACADEMIES[
            next(
                aca for aca in selected_academies if x in ACADEMIES[aca]["departements"]
            )
        ]["departements"][x],
        placeholder="Choisissez vos départements",
    )
    selected_communes = st.multiselect(
        label="Communes",
        options=[
            commune.get("nom")
            for codeDepartement in selected_departements
            for commune in fetch_communes(codeDepartement)
        ],
        placeholder="Choisissez vos communes",
        help="Le choix des départements est nécessaire pour sélectionner les communes",
    )

    noms_communes = [commune for commune in selected_communes if commune is not None]

    selected_type_etablissement = st.multiselect(
        label="Type d'établissement",
        options=TYPE_ETABLISSEMENT,
        placeholder="Choisissez le type d'établissement",
    )
    selected_etablissement_secteur = st.multiselect(
        label="Secteur", options=SECTEUR, placeholder="Secteur des établissements"
    )
    selected_start_effectif, selected_end_effectif = st.select_slider(
        label="Nombre d'élèves par établissement",
        options=[nb for nb in range(0, 3100, 100)],
        value=(0, 3000),
        help="Le filtre ne s'applique pas sur les établissements pour lesquels le nombre d'élèves n'est pas connu",
    )
    # selected_indice_eloignement =
    # uai_selected_education_prioritaire =

    # Tout est centralisé sur la même requête car l'API limite à 100 lignes par appel maximum
    # L'intersection de plusieurs liste ne garantie donc pas l'intégrité de la donnée
    uai_etablissement = fetch_uai_etablissement_filter(
        libelles_academies=selected_academies,
        libelles_departements=selected_departements,
        noms_communes=noms_communes,
        range={"min": selected_start_effectif, "max": selected_end_effectif},
    )

    selected_uai_etablissements = st.multiselect(
        "UAI Etablissements",
        uai_etablissement,
        placeholder="Choisir des établissements",
    )


# Initialisation des variables de session
if "messages" not in st.session_state:
    st.session_state.messages = []

if "session_id" not in st.session_state:
    st.session_state.session_id = None

use_rag = st.toggle(label="Rechercher dans les rapports", value=True)

# Affichage de l'historique de chat
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

# Accept user input
if prompt := st.chat_input("Votre question :"):
    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})
    # Display user message in chat message container
    with st.chat_message("user"):
        display_message_with_feedback(
            content=prompt,
            message_type="user",
            metadata={"uai_etablissement": selected_uai_etablissements},
        )

        # st.markdown(prompt)
        # client_sql.insert_message(
        #     user_id=0,
        #     conv_id=0,
        #     content=prompt,
        #     message_type="user",
        #     meta={"uai_etablissement": selected_uai_etablissements},
        # )

    # Display assistant response in chat message container
    with st.chat_message("assistant"):
        if use_rag is True:
            list_uai_filter = [
                f'metadata LIKE "%{uai}%"' for uai in selected_uai_etablissements
            ]
            uai_filter = " OR ".join(list_uai_filter)
            prompt_history = client_sql.get_conv_history(
                conv_id=0
            )  # TODO Create conv_id in order to make everything works

            print(f"Prompt: {prompt}")
            print(f"Prompt history: {prompt_history}")

            response = requests.post(
                str(RAG_API_URL) + "/query",
                json={
                    "text": prompt,
                    "prompt_history": prompt_history,
                    "filter": uai_filter,
                    "message_id": "TODO",
                },
            )
            time.sleep(5)

            display_message_with_feedback(
                content=response.json()["llm_response"],
                message_type="assistant",
                metadata={"uai_etablissement": selected_uai_etablissements},
            )

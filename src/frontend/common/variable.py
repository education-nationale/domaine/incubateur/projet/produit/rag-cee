import os

from dotenv import load_dotenv

load_dotenv()

RAG_API_URL = os.getenv("RAG_API_URL")
DATABASE_HOST = os.getenv("DATABASE_HOST")
DATABASE_PORT = os.getenv("DATABASE_PORT")
DATABASE_NAME = os.getenv("DATABASE_NAME")
DATABASE_USER = os.getenv("DATABASE_USER")
DATABASE_PWD = os.getenv("DATABASE_PWD")

ACADEMIES = {
    "Aix-Marseille": {
        "departements": {
            "04": "Alpes-de-Haute-Provence",
            "05": "Hautes-Alpes",
            "13": "Bouches-du-Rhône",
            "84": "Vaucluse",
        }
    },
    "Amiens": {"departements": {"02": "Aisne", "60": "Oise", "80": "Somme"}},
    "Besançon": {
        "departements": {
            "25": "Doubs",
            "39": "Jura",
            "70": "Haute-Saône",
            "90": "Territoire de Belfort",
        }
    },
    "Bordeaux": {
        "departements": {
            "24": "Dordogne",
            "33": "Gironde",
            "40": "Landes",
            "47": "Lot-et-Garonne",
            "64": "Pyrénées-Atlantiques",
        }
    },
    "Clermont-Ferrand": {
        "departements": {
            "03": "Allier",
            "15": "Cantal",
            "43": "Haute-Loire",
            "63": "Puy-de-Dôme",
        }
    },
    "Corse": {
        "departements": {
            "2A": "Corse-du-Sud",
            "2B": "Haute-Corse",
        }
    },
    "Créteil": {
        "departements": {
            "77": "Seine-et-Marne",
            "93": "Seine-Saint-Denis",
            "94": "Val-de-Marne",
        }
    },
    "Dijon": {
        "departements": {
            "21": "Côte-d'Or",
            "58": "Nièvre",
            "71": "Saône-et-Loire",
            "89": "Yonne",
        }
    },
    "Grenoble": {
        "departements": {
            "07": "Ardèche",
            "26": "Drôme",
            "38": "Isère",
            "73": "Savoie",
            "74": "Haute-Savoie",
        }
    },
    "la Guadeloupe": {"departements": {"971": "Guadeloupe"}},
    "la Guyane": {"departements": {"973": "Guyane"}},
    "la Martinique": {"departements": {"972": "Martinique"}},
    "La Réunion": {"departements": {"974": "La Réunion"}},
    "Lille": {"departements": {"59": "Nord", "62": "Pas-de-Calais"}},
    "Limoges": {
        "departements": {"19": "Corrèze", "23": "Creuse", "87": "Haute-Vienne"}
    },
    "Lyon": {"departements": {"01": "Ain", "42": "Loire", "69": "Rhône"}},
    "Mayotte": {"departements": {"976": "Mayotte"}},
    "Montpellier": {
        "departements": {
            "11": "Aude",
            "30": "Gard",
            "34": "Hérault",
            "48": "Lozère",
            "66": "Pyrénées-Orientales",
        }
    },
    "Nancy-Metz": {
        "departements": {
            "54": "Meurthe-et-Moselle",
            "55": "Meuse",
            "57": "Moselle",
            "88": "Vosges",
        }
    },
    "Nantes": {
        "departements": {
            "44": "Loire-Atlantique",
            "49": "Maine-et-Loire",
            "53": "Mayenne",
            "72": "Sarthe",
            "85": "Vendée",
        }
    },
    "Nice": {"departements": {"06": "Alpes-Maritimes", "83": "Var"}},
    "Normandie": {
        "departements": {
            "14": "Calvados",
            "27": "Eure",
            "50": "Manche",
            "61": "Orne",
            "76": "Seine-Maritime",
        }
    },
    "Orléans-Tours": {
        "departements": {
            "18": "Cher",
            "28": "Eure-et-Loir",
            "36": "Indre",
            "37": "Indre-et-Loire",
            "41": "Loir-et-Cher",
            "45": "Loiret",
        }
    },
    "Paris": {"departements": {"75": "Paris"}},
    "Poitiers": {
        "departements": {
            "16": "Charente",
            "17": "Charente-Maritime",
            "79": "Deux-Sèvres",
            "86": "Vienne",
        }
    },
    "Reims": {
        "departements": {
            "08": "Ardennes",
            "10": "Aube",
            "51": "Marne",
            "52": "Haute-Marne",
        }
    },
    "Rennes": {
        "departements": {
            "22": "Côtes-d'Armor",
            "29": "Finistère",
            "35": "Ille-et-Vilaine",
            "56": "Morbihan",
        }
    },
    "Strasbourg": {"departements": {"67": "Bas-Rhin", "68": "Haut-Rhin"}},
    "Toulouse": {
        "departements": {
            "09": "Ariège",
            "12": "Aveyron",
            "31": "Haute-Garonne",
            "32": "Gers",
            "46": "Lot",
            "65": "Hautes-Pyrénées",
            "81": "Tarn",
            "82": "Tarn-et-Garonne",
        }
    },
    "Versailles": {
        "departements": {
            "78": "Yvelines",
            "91": "Essonne",
            "92": "Hauts-de-Seine",
            "95": "Val-d'Oise",
        }
    },
}


TYPE_ETABLISSEMENT = ["Collège", "Lycée Générale et Technologie", "Lycée Profesionnel"]

SECTEUR = ["Public", "Privé sous contrat"]

-- Creation of messages table
CREATE TABLE IF NOT EXISTS messages (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT NOT NULL,
    conv_id BIGINT NOT NULL,
    content TEXT NOT NULL,
    meta JSON,
    created_at BIGINT NOT NULL,
    message_type VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Creating indexes for better performance
CREATE INDEX idx_messages_user_id ON messages(user_id);
CREATE INDEX idx_messages_conv_id ON messages(conv_id); -- Corrigé le nom du index
CREATE INDEX idx_messages_created_at ON messages(created_at);

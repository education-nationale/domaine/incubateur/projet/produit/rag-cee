fastapi[standard]==0.115.6
uvicorn
pypdf==5.1.0
langchain_community==0.3.11
langchain_text_splitters==0.3.2
pymilvus==2.5.0
streamlit==1.39.0
